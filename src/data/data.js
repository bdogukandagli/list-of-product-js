import { products } from './products.json';
import { categories } from './categories.json';

export const filterData = categories;
export const productData = products;
