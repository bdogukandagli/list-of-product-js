import React, { useState, useContext, useEffect } from 'react';
import hamburgerIcon from '../../../static/category-hamburger.svg';
import CategoryTag from '../../../components/categoryTag/index';
import ProductCard from '../../../components/productCard/index';
import leafIcon from '../../../static/leaf-icon.svg';
import { ProductContext } from '../../../context/productContext';

const List = ({ productCart, setProductCart, keyword, setKeyword }) => {
  const { getProducts, getFilters, getFilteredProducts, filters, products } = useContext(
    ProductContext
  );
  const [selectedFilters, setSelectedFilters] = useState([]);

  useEffect(() => {
    getProducts();
    getFilters();
  }, []);

  useEffect(() => {
    if (keyword?.length > 3) {
      getFilteredProducts(selectedFilters, keyword);
    } else {
      getFilteredProducts(selectedFilters, keyword);
    }
  }, [keyword]);

  useEffect(() => {
    if (selectedFilters.length > 0) {
      getFilteredProducts(selectedFilters, keyword);
    } else {
      getProducts();
    }
  }, [selectedFilters]);

  const handleFiltersArray = (text) => {
    if (selectedFilters.includes(text)) {
      setSelectedFilters(selectedFilters.filter((t) => t != text));
    } else {
      setSelectedFilters(selectedFilters.concat([text]));
    }
  };

  const renderCategories = () => {
    return (
      <div className="Wrapper">
        <div className="Categories">
          <div className="Categories-heading">
            <div>
              <img src={hamburgerIcon} />
            </div>
            <div>
              <p className="Categories-heading-text">Kategoriler</p>
            </div>
          </div>
        </div>
        <div className="Category-Tag-wrapper">
          <CategoryTag text={'Tüm Kategoriler'} isActive={selectedFilters.length == 0} />
          {filters.map((filter) => {
            return (
              <div key={filter.id} onClick={() => handleFiltersArray(filter.text)}>
                <CategoryTag
                  text={filter.text}
                  isActive={selectedFilters?.includes(filter.text)}
                />
              </div>
            );
          })}
        </div>
      </div>
    );
  };

  const renderResults = () => {
    return (
      <div className="Wrapper">
        <div className="products-label">
          <img src={leafIcon} className="products-label-icon" />
          <p>Tüm Kategoriler</p>
        </div>
        <div className="products-root">
          {products.map((product) => {
            return (
              <ProductCard
                key={product.id}
                photoUrl={product.photoUrl}
                name={product.name}
                isFreeShipping={product.isFreeShipping}
                price={product.price}
                productCart={productCart}
                setProductCart={setProductCart}
                product={product}
              />
            );
          })}
        </div>
      </div>
    );
  };

  return (
    <div>
      {renderCategories()}
      {renderResults()}
    </div>
  );
};

export default List;
