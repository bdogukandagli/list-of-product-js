import React from 'react';
import PhoneIcon from '../../../static/phone-image.png';
import CodeIcon from '../../../static/code-icon.png';
import googlePlay from '../../../static/google-play-icon.svg';
import appStore from '../../../static/app-store-icon.svg';
import headerIcon from '../../../static/header-icon.svg';
import facebookIcon from '../../../static/facebook-icon.svg';
import twitterIcon from '../../../static/twitter-icon.svg';
import blogIcon from '../../../static/blog-icon.svg';
import youtubeIcon from '../../../static/youtube-icon.svg';
import instagramIcon from '../../../static/instagram-icon.svg';

const Footer = () => {
  return (
    <>
      <div className="Footer">
        <div className="Mobile-Section">
          <div className="Mobile-Section-Hero">
            <img src={PhoneIcon} />
          </div>
          <div>
            <div className="Mobile-Section-Wrapper">
              <div>
                <img src={CodeIcon} />
              </div>
              <div>
                <div>
                  <p>Çiçek Sepeti Mobil Uygulamayı İndirin</p>
                </div>
                <div className="Mobile-Section-Subtext">
                  <p>Mobil Uygulamayı QR Kod ile İndirin.</p>
                </div>
              </div>
            </div>
            <div className="Mobile-Section-Icons">
              <div>
                <img src={googlePlay} />
              </div>
              <div>
                <img src={appStore} />
              </div>
            </div>
          </div>
        </div>
        <div className="Titles">
          <div className="Titles-Social-Media">
            <img src={headerIcon} />
            <div className="Titles-Social-Media-Icons">
              <img src={facebookIcon} />
              <img src={twitterIcon} />
              <img src={instagramIcon} />
              <img src={youtubeIcon} />
              <img src={blogIcon} />
            </div>
            <div>
              <p>
                CicekSepeti.com olarak kişisel verilerinizin gizliliğini önemsiyoruz. 6698
                sayılı Kişisel Verilerin Korunması Kanunu kapsamında oluşturduğumuz
                aydınlatma metnine buradan ulaşabilirsiniz.
              </p>
            </div>
          </div>
          <div className="Titles-Divider" />
          <div className="Titles-Text">
            <div>
              <div className="Titles-Text-Heading">
                <p>Faydalı Bilgiler</p>
              </div>
              <div className="Titles-Text-Subtext">
                <p>Çiçek Bakımı</p>
                <p>Çiçek Eşliğinde Notlar</p>
                <p>Çiçek Anlamları</p>
                <p>Özel Günler</p>
                <p>Mevsimlere Göre Çiçekler</p>
                <p>BonnyFood Saklama Koşulları</p>
                <p>Site Haritası</p>
              </div>
            </div>
            <div>
              <div className="Titles-Text-Heading">
                <p>Kurumsal</p>
              </div>
              <div className="Titles-Text-Subtext">
                <p>Hakkımızda</p>
                <p>Kariyer</p>
                <p>ÇiçekSepeti’nde Satış Yap</p>
                <p>Kurumsal Müşterilerimiz</p>
                <p>Reklamlarımız</p>
                <p>Basında Biz</p>
                <p>Kampanyalar</p>
                <p>Vizyonumuz</p>
              </div>
            </div>
            <div>
              <div className="Titles-Text-Heading">
                <p>İletişim</p>
              </div>
              <div className="Titles-Text-Subtext">
                <p>Bize Ulaşın</p>
                <p>Sıkça Sorulan Sorular</p>
              </div>
            </div>
            <div>
              <div className="Titles-Text-Heading">
                <p>Gizlilik Sözleşmesi</p>
              </div>
              <div className="Titles-Text-Subtext">
                <p>Mesafeli Satış Sözleşmesi</p>
                <p>Bilgi Toplumu Hizmetleri</p>
                <p>Gizlilik Sözleşmesi</p>
                <p>Ödeme Seçenekleri</p>
                <p>Hesap Bilgileri</p>
              </div>
            </div>
          </div>
        </div>
        <div className="Description">
          <p>
            Türkiyenin en büyük online çiçek sitesi ÇiçekSepeti ile sevdiklerinizi mutlu
            etmek çok kolay! Çiçek göndermenin ve “Mutlu etmenin adresi” Çiçek Sepeti ile
            sevdiklerinize özel günlerde online çiçek gönderebilirsiniz. Geniş çiçekçi ağı
            sayesinde çiçek siparişleriniz Türkiye’nin dört bir yanına hızlı ve sorunsuz
            şekilde gönderilir. Taze çiçeklerle hazırlanan mis kokulu şık çiçek
            aranjmanları arasından beğendiğiniz ürünü seçerek, hızlı bir şekilde online
            sipariş verebilirsiniz. Sevdiklerinizin doğum günü, yıldönümü gibi mutlu
            günlerinde onların sevincine ortak olmak için yapmanız gereken sadece birkaç
            tıkla sipariş vermek. Sevgililer Günü, Kadınlar Günü, Anneler Günü gibi özel
            günlerde de çiçek, hediye ve lezzetli bonnyFood ürünleriyle sevdiklerinizi
            mutlu edebilirsiniz. Çünkü mutlu etmenin adresi; ÇiçekSepeti.
          </p>
        </div>
      </div>
      <div className="Copyright">
        <p>Copyright © 2019 Çiçek Sepeti İnternet Hizmetleri A.Ş</p>
      </div>
    </>
  );
};

export default Footer;
