import React from 'react';
import styled from 'styled-components';
import motoIcon from '../../../static/feature-icon-moto.png';
import giftIcon from '../../../static/feature-icon-gift.png';
import paperIcon from '../../../static/feature-icon-paper.png';

const FeatureSection = () => {
  return (
    <div>
      <div className="Divider" />
      <div className="Features">
        <div className="Features-Card-Danger">
          <div>
            <img src={motoIcon} />
          </div>
          <div>
            <p>75 TL Üzerine Teslimat Ücreti Bizden</p>
            <div className="card-button">
              <p>Detaylı Bilgi</p>
            </div>
          </div>
        </div>
        <div className="Features-Card-Custom">
          <div>
            <img src={giftIcon} />
          </div>
          <div>
            <p>Hediye Kategorisi için Sepette %15 İndirim</p>
            <div className="card-button">
              <p>Detaylı Bilgi</p>
            </div>
          </div>
        </div>
        <div className="Features-Card-Success">
          <div>
            <img src={paperIcon} />
          </div>
          <div>
            <p>Kırtasiye Kategorisi için Sepette %15 İndirim</p>
            <div className="card-button">
              <p>Detaylı Bilgi</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FeatureSection;
