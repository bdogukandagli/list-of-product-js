import React, { useState } from 'react';
import './App.scss';
import Header from '../components/header/index';
import Banner from '../components/banner/index';
import List from './containers/list';
import FeatureSection from './containers/featureSection';
import Footer from './containers/footer';

const App = () => {
  const [productCart, setProductCart] = useState([]);
  const [keyword, setKeyword] = useState();

  return (
    <div className="App">
      <Header
        productCart={productCart}
        setProductCart={setProductCart}
        keyword={keyword}
        setKeyword={setKeyword}
      />
      <Banner />
      <List
        productCart={productCart}
        setProductCart={setProductCart}
        keyword={keyword}
        setKeyword={setKeyword}
      />
      <FeatureSection />
      <Footer />
    </div>
  );
};

export default App;
