import {
  GET_PRODUCTS_ERROR,
  GET_PRODUCTS_START,
  GET_PRODUCTS_SUCCESS,
  GET_FILTERS_ERROR,
  GET_FILTERS_SUCCESS,
  GET_FILTERS_START,
  GET_FILTERED_PRODUCTS_ERROR,
  GET_FILTERED_PRODUCTS_SUCCESS,
  GET_FILTERED_PRODUCTS_START,
} from './types';

export const reducer = (state, action) => {
  switch (action.type) {
    case GET_PRODUCTS_START:
      return {
        ...state,
      };
    case GET_PRODUCTS_SUCCESS:
      return {
        ...state,
        products: action.payload,
      };
    case GET_PRODUCTS_ERROR:
      return {
        ...state,
      };
    case GET_FILTERS_START:
      return {
        ...state,
      };
    case GET_FILTERS_SUCCESS:
      return {
        ...state,
        filters: action.payload,
      };
    case GET_FILTERS_ERROR:
      return {
        ...state,
      };
    case GET_FILTERED_PRODUCTS_START:
      return {
        ...state,
      };
    case GET_FILTERED_PRODUCTS_SUCCESS:
      return {
        ...state,
        products: action.payload,
      };
    case GET_FILTERED_PRODUCTS_ERROR:
      return {
        ...state,
      };
    default:
      return state;
  }
};
