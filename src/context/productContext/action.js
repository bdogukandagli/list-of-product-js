import React, { useReducer } from 'react';
import {
  GET_PRODUCTS_ERROR,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCTS_START,
  GET_FILTERS_ERROR,
  GET_FILTERS_START,
  GET_FILTERS_SUCCESS,
  GET_FILTERED_PRODUCTS_ERROR,
  GET_FILTERED_PRODUCTS_START,
  GET_FILTERED_PRODUCTS_SUCCESS,
} from './types';
import { reducer } from './reducer';
import { createContext } from 'react';
import { productData, filterData } from '../../data/data';

const Context = createContext();

const initialState = {
  error: '',
  isLoading: false,
  products: [],
  filters: [],
  isProductRequestDone: false,
};

const State = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const getProducts = async () => {
    dispatch({
      type: GET_PRODUCTS_START,
    });
    try {
      const data = productData;
      dispatch({
        type: GET_PRODUCTS_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: GET_PRODUCTS_ERROR,
        payload: error.stack ? error.stack : 'Network Error !',
      });
    }
  };

  const getFilters = async () => {
    dispatch({
      type: GET_FILTERS_START,
    });
    try {
      const data = filterData;
      dispatch({
        type: GET_FILTERS_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: GET_FILTERS_ERROR,
        payload: error.stack ? error.stack : 'Network Error !',
      });
    }
  };

  const getFilteredProducts = async (categories, keyword) => {
    dispatch({
      type: GET_FILTERED_PRODUCTS_START,
    });
    try {
      let data = productData;
      if (categories.length > 0) {
        data = data.filter(function (item) {
          return categories.indexOf(item.category) !== -1;
        });
      }
      if (keyword) {
        if (keyword.length > 3) {
          data = data.filter((e) => e.name.includes(keyword) === true);
        }
      }

      dispatch({
        type: GET_FILTERED_PRODUCTS_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: GET_FILTERED_PRODUCTS_ERROR,
        payload: error.stack ? error.stack : 'Network Error !',
      });
    }
  };

  return (
    <Context.Provider
      value={{
        isLoading: state.isLoading,
        error: state.error,
        isProductRequestDone: state.isProductRequestDone,
        products: state.products,
        filters: state.filters,
        getProducts,
        getFilters,
        getFilteredProducts,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export { State, Context };
