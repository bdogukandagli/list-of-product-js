import React from 'react';
import { ProductState } from '../productContext';

export const Wrapper = ({ children }) => {
  return <ProductState>{children}</ProductState>;
};
