import React from 'react';

const CategoryTag = ({ text, isActive }) => {
  return (
    <div className="Category-Tag">
      <div className={isActive ? 'Category-Tag-Active' : 'Category-Tag-Passive'}>
        <p>{text}</p>
      </div>
    </div>
  );
};

export default CategoryTag;
