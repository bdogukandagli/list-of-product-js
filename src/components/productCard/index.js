import React from 'react';

const ProductCard = ({
  photoUrl,
  name,
  isFreeShipping,
  price,
  productCart,
  setProductCart,
  product,
}) => {
  return (
    <div className="Product-Card">
      <div>
        <img src={photoUrl} className="Product-Card-photo" />
      </div>
      <div>
        <p className="Product-Card-name">{name}</p>
      </div>
      <div className="Product-Card-subtext-wrapper">
        <p className="Product-Card-subtext">
          {isFreeShipping ? 'Ücretsiz Teslimat' : ''}
        </p>
      </div>
      <div className="Product-Card-price-wrapper">
        <p className="Product-Card-price">{`${price}₺`}</p>
      </div>
      <div
        className="Product-Card-button-wrapper"
        onClick={() => setProductCart((currentProducts) => [...currentProducts, product])}
      >
        <p className="Product-Card-button">Sepete Ekle</p>
      </div>
    </div>
  );
};

export default ProductCard;
