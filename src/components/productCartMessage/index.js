import React, { useState, useEffect } from 'react';
import styled from 'styled-components/macro';
import zIcon from '../../static/z-icon.svg';

const ProductCartMessage = ({ isActive, productCart, ...props }) => {
  const [totalPrice, setTotalPrice] = useState(0);

  useEffect(() => {
    calculateTotalPrice();
  }, [productCart]);

  const calculateTotalPrice = () => {
    productCart.forEach((cp) => {
      setTotalPrice(totalPrice + cp.price);
    });
  };

  return (
    <div className="Product-Cart-Message-wrapper">
      <div className="Product-Cart-Message-box" />
      <div>
        <div className="Product-Cart-Message-content">
          <div className="Product-Cart-Message-content-box">
            <div>
              <img src={zIcon} />
            </div>
            <div style={{ marginLeft: 10 }}>
              <div>
                {totalPrice <= 500 ? (
                  <p className="Product-Cart-Message-content-text">
                    <span style={{ color: '#FFCE00' }}>
                      {(500 - totalPrice).toFixed(2)} TL
                    </span>{' '}
                    ürün daha ekleyin kargo bedava
                  </p>
                ) : (
                  <p className="Product-Cart-Message-content-text">Kargonuz Bedava</p>
                )}
              </div>
              <div className="Product-Cart-Message-ProgressBar-Wrapper">
                <div
                  className="Product-Cart-Message-ProgressBar-Progress"
                  css={`
                    width: ${parseInt(totalPrice) / 5}%;
                    max-width: 230px;
                  `}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductCartMessage;
