import React from 'react';
import headerIcon from '../../static/header-icon.svg';
import inputSearchIcon from '../../static/input-search-icon.svg';
import cartIcon from '../../static/header-cart-icon.svg';
import ProductCartMessage from '../productCartMessage/index';

const Header = ({ productCart, keyword, setKeyword }) => {
  const mystyle = {
    maxWidth: '228px',
    marginRight: '-80px',
    marginTop: '100px',
    position: 'absolute',
  };

  return (
    <div className="Header">
      <div className="Header-icon">
        <img src={headerIcon} />
      </div>
      <div className="Header-input-wrapper">
        <div className="Header-input-flex">
          <div>
            <img className="Header-input-icon" src={inputSearchIcon} />
          </div>
          <div>
            <input
              className="Header-input-box"
              placeholder="Ürün Ara"
              onChange={(e) => setKeyword(e.target.value)}
            />
          </div>
        </div>
        <div className="Header-input-button">
          <p className="Header-input-button-text">Ara</p>
        </div>
      </div>
      <div className="Header-cart-wrapper">
        <img src={cartIcon} />
        <p className="Header-cart-text">Sepetim</p>
        <div className="Counter">
          <p className="Header-cart-text">{productCart.length}</p>
        </div>
        <div style={mystyle}>
          <ProductCartMessage isActive={true} productCart={productCart} />
        </div>
      </div>
    </div>
  );
};

export default Header;
