import React from 'react';

const Banner = () => {
  return (
    <div className="Banner">
      <div className="Banner-wrapper">
        <p className="Banner-heading">ÇiçekSepeti H1</p>
      </div>
      <div className="Banner-subtitle">
        <p>
          Ciceksepeti Market &gt; &nbsp; İstanbul &gt; &nbsp;
          <span style={{ color: '#51B549' }}>CiceksepetiBreadCrumb</span>
        </p>
      </div>
    </div>
  );
};

export default Banner;
